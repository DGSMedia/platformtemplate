extends KinematicBody2D

export var speed = 200
export var jump_speed = -1000
export var gravity = 2000

var motion : Vector2 = Vector2.ZERO

func _process(delta):
	handleInput()
	motion.y += gravity * delta
	motion = move_and_slide(motion,Vector2.UP)
		

func handleInput():
	motion.x = 0
	if Input.is_action_pressed("ui_left"):
		motion.x -= speed
	elif Input.is_action_pressed("ui_right"):
		motion.x += speed
		
	if Input.is_action_just_pressed("jump"):
		if is_on_floor():
			motion.y = jump_speed

func die():
	queue_free()

func _on_Hitbox_body_entered(body):
	if body.is_in_group("enemy"):
		die()
	
