extends KinematicBody2D
var speed = 200
var direction = 1
var motion = Vector2.ZERO
var gravity = 2000

onready var raycast = $RayCast2D

func _ready():
	pass # Replace with function body.
	
func _process(delta):
	motion.x = speed * direction
	motion.y += gravity * delta
	motion = move_and_slide(motion)
	
	
	if raycast.is_colliding():
		var target = raycast.get_collider()
		direction = direction * -1
		raycast.cast_to.x = raycast.cast_to.x * -1 
